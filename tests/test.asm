.eqv PRINT_INT  1
.eqv PRINT_STR  4
.eqv PRINT_CHAR 11
.eqv READ_INT   5
.eqv READ_CHAR  12

.data
	_str18: .asciiz "invalid num! num must larger than 0."
	_str19: .asciiz "found "
	_str78: .asciiz "should not happen"
	_str104: .asciiz "calc fib num, input:"
	_str105: .asciiz "try calc fib "
	_str106: .asciiz "result is "
	_err: .asciiz "runtime error: "
	_tmp4_errorFlag: .word 47
	_tmp6_f: .space 200
.text
	j _L2_programBegin
_L8_check:
	sw $ra, -4($sp)
	subiu $sp, $sp, 12
	lw $t2, 4($sp)  # _tmp9_num
	li $t3, 0
	sle $t1, $t2, $t3
	sw $t1, 0($sp)  # _tmp11
	lw $t1, 0($sp)  # _tmp11
	li $t2, 0
	beq $t1, $t2, _L13_endOfIf
	lw $v0, _tmp4_errorFlag
	addiu $sp, $sp, 12
	lw $ra, -4($sp)
	jr $ra
_L13_endOfIf:
	li $v0, 43
	addiu $sp, $sp, 12
	lw $ra, -4($sp)
	jr $ra
_L15_error:
	sw $ra, -4($sp)
	subiu $sp, $sp, 12
	la $a0, _str18
	li $v0, PRINT_STR
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	la $a0, _str19
	li $v0, PRINT_STR
	syscall
	lw $a0, 0($sp)  # _tmp17_num
	li $v0, PRINT_INT
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	addiu $sp, $sp, 12
	lw $ra, -4($sp)
	jr $ra
_L20_fib:
	sw $ra, -4($sp)
	subiu $sp, $sp, 92
	li $t0, 0
	sw $t0, 80($sp)  # _tmp23_result
	lw $t0, 84($sp)  # _tmp21_num
	sw $t0, -8($sp)
	jal _L8_check
	sw $v0, 76($sp)  # _tmp24
	lw $t0, 76($sp)  # _tmp24
	sw $t0, 80($sp)  # _tmp23_result
	lw $t1, 80($sp)  # _tmp23_result
	li $t2, 47
	beq $t1, $t2, _L27_Case
	lw $t1, 80($sp)  # _tmp23_result
	li $t2, 43
	beq $t1, $t2, _L29_Case
	j _L30_Default
_L27_Case:
	lw $t0, 80($sp)  # _tmp23_result
	sw $t0, -8($sp)
	lw $t0, 84($sp)  # _tmp21_num
	sw $t0, -12($sp)
	jal _L15_error
	li $v0, 0
	addiu $sp, $sp, 92
	lw $ra, -4($sp)
	jr $ra
	j _L25_endOfSwitch
_L29_Case:
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 3
	sge $t1, $t2, $t3
	sw $t1, 72($sp)  # _tmp33
	lw $t1, 72($sp)  # _tmp33
	li $t2, 0
	beq $t1, $t2, _L35_endOfIf
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 2
	sub $t1, $t2, $t3
	sw $t1, 68($sp)  # _tmp37
	lw $t1, 68($sp)  # _tmp37
	sll $t1, $t1, 2
	lw $t0, _tmp6_f($t1)
	sw $t0, 64($sp)  # _tmp39
	lw $t2, 64($sp)  # _tmp39
	li $t3, 0
	seq $t1, $t2, $t3
	sw $t1, 60($sp)  # _tmp41
	lw $t1, 60($sp)  # _tmp41
	li $t2, 0
	beq $t1, $t2, _L43_endOfIf
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 2
	sub $t1, $t2, $t3
	sw $t1, 56($sp)  # _tmp45
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 2
	sub $t1, $t2, $t3
	sw $t1, 52($sp)  # _tmp48
	lw $t0, 52($sp)  # _tmp48
	sw $t0, -8($sp)
	jal _L20_fib
	sw $v0, 48($sp)  # _tmp50
	lw $t0, 48($sp)  # _tmp50
	lw $t1, 56($sp)  # _tmp45
	sll $t1, $t1, 2
	sw $t0, _tmp6_f($t1)
_L43_endOfIf:
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 44($sp)  # _tmp52
	lw $t1, 44($sp)  # _tmp52
	sll $t1, $t1, 2
	lw $t0, _tmp6_f($t1)
	sw $t0, 40($sp)  # _tmp54
	lw $t2, 40($sp)  # _tmp54
	li $t3, 0
	seq $t1, $t2, $t3
	sw $t1, 36($sp)  # _tmp56
	lw $t1, 36($sp)  # _tmp56
	li $t2, 0
	beq $t1, $t2, _L58_endOfIf
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 32($sp)  # _tmp60
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 28($sp)  # _tmp63
	lw $t0, 28($sp)  # _tmp63
	sw $t0, -8($sp)
	jal _L20_fib
	sw $v0, 24($sp)  # _tmp65
	lw $t0, 24($sp)  # _tmp65
	lw $t1, 32($sp)  # _tmp60
	sll $t1, $t1, 2
	sw $t0, _tmp6_f($t1)
_L58_endOfIf:
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 20($sp)  # _tmp67
	lw $t1, 20($sp)  # _tmp67
	sll $t1, $t1, 2
	lw $t0, _tmp6_f($t1)
	sw $t0, 16($sp)  # _tmp69
	lw $t2, 84($sp)  # _tmp21_num
	li $t3, 2
	sub $t1, $t2, $t3
	sw $t1, 12($sp)  # _tmp71
	lw $t1, 12($sp)  # _tmp71
	sll $t1, $t1, 2
	lw $t0, _tmp6_f($t1)
	sw $t0, 8($sp)  # _tmp73
	lw $t2, 16($sp)  # _tmp69
	lw $t3, 8($sp)  # _tmp73
	add $t1, $t2, $t3
	sw $t1, 4($sp)  # _tmp74
	lw $t0, 4($sp)  # _tmp74
	lw $t1, 84($sp)  # _tmp21_num
	sll $t1, $t1, 2
	sw $t0, _tmp6_f($t1)
	lw $t1, 84($sp)  # _tmp21_num
	sll $t1, $t1, 2
	lw $t0, _tmp6_f($t1)
	sw $t0, 0($sp)  # _tmp76
	lw $v0, 0($sp)  # _tmp76
	addiu $sp, $sp, 92
	lw $ra, -4($sp)
	jr $ra
_L35_endOfIf:
	li $v0, 1
	addiu $sp, $sp, 92
	lw $ra, -4($sp)
	jr $ra
	j _L25_endOfSwitch
_L30_Default:
	la $a0, _str78
	li $v0, PRINT_STR
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	j _L25_endOfSwitch
_L25_endOfSwitch:
	la $a0, _err
	li $v0, 4
	syscall
	li $a0, 0
	li $v0, 1
	syscall
	li $a0, 0
	li $v0, 17
	syscall
_L80_main:
	sw $ra, -4($sp)
	subiu $sp, $sp, 28
	li $t0, 0
	sw $t0, 20($sp)  # _tmp82_n
	li $t0, 0
	sw $t0, 16($sp)  # _tmp84_i
	li $t2, 0
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 12($sp)  # _tmp88
	lw $t0, 12($sp)  # _tmp88
	li $t1, 0
	sll $t1, $t1, 2
	sw $t0, _tmp6_f($t1)
	li $t0, 1
	li $t1, 1
	sll $t1, $t1, 2
	sw $t0, _tmp6_f($t1)
	li $t0, 1
	li $t1, 2
	sll $t1, $t1, 2
	sw $t0, _tmp6_f($t1)
	li $t0, 0
	sw $t0, 16($sp)  # _tmp84_i
_L95_whileBeginTest:
	lw $t2, 16($sp)  # _tmp84_i
	li $t3, 50
	slt $t1, $t2, $t3
	sw $t1, 8($sp)  # _tmp97
	lw $t1, 8($sp)  # _tmp97
	li $t2, 0
	beq $t1, $t2, _L99_endOfWhile
	li $t0, 0
	lw $t1, 16($sp)  # _tmp84_i
	sll $t1, $t1, 2
	sw $t0, _tmp6_f($t1)
	lw $t2, 16($sp)  # _tmp84_i
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 4($sp)  # _tmp102
	lw $t0, 4($sp)  # _tmp102
	sw $t0, 16($sp)  # _tmp84_i
	j _L95_whileBeginTest
_L99_endOfWhile:
	la $a0, _str104
	li $v0, PRINT_STR
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	li $v0, READ_INT
	syscall
	sw $v0, 20($sp)  # _tmp82_n
	la $a0, _str105
	li $v0, PRINT_STR
	syscall
	lw $a0, 20($sp)  # _tmp82_n
	li $v0, PRINT_INT
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	lw $t0, 20($sp)  # _tmp82_n
	sw $t0, -8($sp)
	jal _L20_fib
	sw $v0, 0($sp)  # _tmp107
	la $a0, _str106
	li $v0, PRINT_STR
	syscall
	lw $a0, 0($sp)  # _tmp107
	li $v0, PRINT_INT
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	addiu $sp, $sp, 28
	lw $ra, -4($sp)
	jr $ra
_L2_programBegin:
	jal _L80_main
	j _L108_endOfProgram
_L1_checkConvert:
	sw $ra, -4($sp)
	subiu $sp, $sp, 8
	lw $t1, 0($sp)  # _tmp109
	li $t2, 42
	beq $t1, $t2, _L114_endOfCheck
	lw $t1, 0($sp)  # _tmp109
	li $t2, 43
	beq $t1, $t2, _L114_endOfCheck
	lw $t1, 0($sp)  # _tmp109
	li $t2, 45
	beq $t1, $t2, _L114_endOfCheck
	lw $t1, 0($sp)  # _tmp109
	li $t2, 47
	bge $t1, $t2, _L110_check57
	j _L113_error
_L110_check57:
	lw $t1, 0($sp)  # _tmp109
	li $t2, 57
	ble $t1, $t2, _L114_endOfCheck
	lw $t1, 0($sp)  # _tmp109
	li $t2, 65
	bge $t1, $t2, _L111_check90
	j _L113_error
_L111_check90:
	lw $t1, 0($sp)  # _tmp109
	li $t2, 90
	ble $t1, $t2, _L114_endOfCheck
	lw $t1, 0($sp)  # _tmp109
	li $t2, 97
	bge $t1, $t2, _L112_check122
	j _L113_error
_L112_check122:
	lw $t1, 0($sp)  # _tmp109
	li $t2, 122
	ble $t1, $t2, _L114_endOfCheck
_L113_error:
	la $a0, _err
	li $v0, 4
	syscall
	li $a0, 1
	li $v0, 1
	syscall
	li $a0, 1
	li $v0, 17
	syscall
_L114_endOfCheck:
	addiu $sp, $sp, 8
	lw $ra, -4($sp)
	jr $ra
_L108_endOfProgram:
	li $a0, 0
	li $v0, 17
	syscall
