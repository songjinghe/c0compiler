.eqv PRINT_INT 1
.eqv PRINT_STR 4
.eqv PRINT_CHAR 11
.eqv READ_INT 5
.eqv READ_CHAR 12
.data
	_err: .asciiz "runtime error: "
	_str137: .asciiz " , "
	_tmp4_d: .space 80
.text
	j _L2_programBegin
_L6_sort:
	sw $ra, -4($sp)
	subiu $sp, $sp, 140
	li $t0, 0
	sw $t0, 124($sp)  # _tmp10_x
	li $t0, 0
	sw $t0, 120($sp)  # _tmp12_y
	li $t0, 0
	sw $t0, 116($sp)  # _tmp14_z
	li $t0, 0
	sw $t0, 112($sp)  # _tmp16_tmp
	lw $t0, 132($sp)  # _tmp7_i
	sw $t0, 124($sp)  # _tmp10_x
	lw $t0, 128($sp)  # _tmp8_j
	sw $t0, 120($sp)  # _tmp12_y
	lw $t2, 132($sp)  # _tmp7_i
	lw $t3, 128($sp)  # _tmp8_j
	add $t1, $t2, $t3
	sw $t1, 108($sp)  # _tmp17
	lw $t2, 108($sp)  # _tmp17
	li $t3, 2
	div $t1, $t2, $t3
	sw $t1, 104($sp)  # _tmp20
	lw $t1, 104($sp)  # _tmp20
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 100($sp)  # _tmp21
	lw $t0, 100($sp)  # _tmp21
	sw $t0, 116($sp)  # _tmp14_z
_L22_whileBeginTest:
	lw $t1, 120($sp)  # _tmp12_y
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 96($sp)  # _tmp23
	lw $t2, 116($sp)  # _tmp14_z
	lw $t3, 96($sp)  # _tmp23
	slt $t1, $t2, $t3
	sw $t1, 92($sp)  # _tmp24
	lw $t1, 92($sp)  # _tmp24
	li $t2, 0
	beq $t1, $t2, _L26_endOfWhile
	lw $t2, 120($sp)  # _tmp12_y
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 88($sp)  # _tmp28
	lw $t0, 88($sp)  # _tmp28
	sw $t0, 120($sp)  # _tmp12_y
	j _L22_whileBeginTest
_L26_endOfWhile:
_L30_whileBeginTest:
	lw $t1, 124($sp)  # _tmp10_x
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 84($sp)  # _tmp31
	lw $t2, 116($sp)  # _tmp14_z
	lw $t3, 84($sp)  # _tmp31
	sgt $t1, $t2, $t3
	sw $t1, 80($sp)  # _tmp32
	lw $t1, 80($sp)  # _tmp32
	li $t2, 0
	beq $t1, $t2, _L34_endOfWhile
	lw $t2, 124($sp)  # _tmp10_x
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 76($sp)  # _tmp36
	lw $t0, 76($sp)  # _tmp36
	sw $t0, 124($sp)  # _tmp10_x
	j _L30_whileBeginTest
_L34_endOfWhile:
	lw $t2, 124($sp)  # _tmp10_x
	lw $t3, 120($sp)  # _tmp12_y
	sle $t1, $t2, $t3
	sw $t1, 72($sp)  # _tmp38
	lw $t1, 72($sp)  # _tmp38
	li $t2, 0
	beq $t1, $t2, _L40_endOfIf
	lw $t1, 124($sp)  # _tmp10_x
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 68($sp)  # _tmp41
	lw $t0, 68($sp)  # _tmp41
	sw $t0, 112($sp)  # _tmp16_tmp
	lw $t1, 120($sp)  # _tmp12_y
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 64($sp)  # _tmp42
	lw $t0, 64($sp)  # _tmp42
	lw $t1, 124($sp)  # _tmp10_x
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	lw $t0, 112($sp)  # _tmp16_tmp
	lw $t1, 120($sp)  # _tmp12_y
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	lw $t2, 124($sp)  # _tmp10_x
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 60($sp)  # _tmp44
	lw $t0, 60($sp)  # _tmp44
	sw $t0, 124($sp)  # _tmp10_x
	lw $t2, 120($sp)  # _tmp12_y
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 56($sp)  # _tmp47
	lw $t0, 56($sp)  # _tmp47
	sw $t0, 120($sp)  # _tmp12_y
_L40_endOfIf:
_L49_whileBeginTest:
	lw $t2, 124($sp)  # _tmp10_x
	lw $t3, 120($sp)  # _tmp12_y
	sle $t1, $t2, $t3
	sw $t1, 52($sp)  # _tmp50
	lw $t1, 52($sp)  # _tmp50
	li $t2, 0
	beq $t1, $t2, _L52_endOfWhile
_L53_whileBeginTest:
	lw $t1, 120($sp)  # _tmp12_y
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 48($sp)  # _tmp54
	lw $t2, 116($sp)  # _tmp14_z
	lw $t3, 48($sp)  # _tmp54
	slt $t1, $t2, $t3
	sw $t1, 44($sp)  # _tmp55
	lw $t1, 44($sp)  # _tmp55
	li $t2, 0
	beq $t1, $t2, _L57_endOfWhile
	lw $t2, 120($sp)  # _tmp12_y
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 40($sp)  # _tmp59
	lw $t0, 40($sp)  # _tmp59
	sw $t0, 120($sp)  # _tmp12_y
	j _L53_whileBeginTest
_L57_endOfWhile:
_L61_whileBeginTest:
	lw $t1, 124($sp)  # _tmp10_x
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 36($sp)  # _tmp62
	lw $t2, 116($sp)  # _tmp14_z
	lw $t3, 36($sp)  # _tmp62
	sgt $t1, $t2, $t3
	sw $t1, 32($sp)  # _tmp63
	lw $t1, 32($sp)  # _tmp63
	li $t2, 0
	beq $t1, $t2, _L65_endOfWhile
	lw $t2, 124($sp)  # _tmp10_x
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 28($sp)  # _tmp67
	lw $t0, 28($sp)  # _tmp67
	sw $t0, 124($sp)  # _tmp10_x
	j _L61_whileBeginTest
_L65_endOfWhile:
	lw $t2, 124($sp)  # _tmp10_x
	lw $t3, 120($sp)  # _tmp12_y
	sle $t1, $t2, $t3
	sw $t1, 24($sp)  # _tmp69
	lw $t1, 24($sp)  # _tmp69
	li $t2, 0
	beq $t1, $t2, _L71_endOfIf
	lw $t1, 124($sp)  # _tmp10_x
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 20($sp)  # _tmp72
	lw $t0, 20($sp)  # _tmp72
	sw $t0, 112($sp)  # _tmp16_tmp
	lw $t1, 120($sp)  # _tmp12_y
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 16($sp)  # _tmp73
	lw $t0, 16($sp)  # _tmp73
	lw $t1, 124($sp)  # _tmp10_x
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	lw $t0, 112($sp)  # _tmp16_tmp
	lw $t1, 120($sp)  # _tmp12_y
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	lw $t2, 124($sp)  # _tmp10_x
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 12($sp)  # _tmp75
	lw $t0, 12($sp)  # _tmp75
	sw $t0, 124($sp)  # _tmp10_x
	lw $t2, 120($sp)  # _tmp12_y
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 8($sp)  # _tmp78
	lw $t0, 8($sp)  # _tmp78
	sw $t0, 120($sp)  # _tmp12_y
_L71_endOfIf:
	j _L49_whileBeginTest
_L52_endOfWhile:
	lw $t2, 124($sp)  # _tmp10_x
	lw $t3, 128($sp)  # _tmp8_j
	slt $t1, $t2, $t3
	sw $t1, 4($sp)  # _tmp80
	lw $t1, 4($sp)  # _tmp80
	li $t2, 0
	beq $t1, $t2, _L82_endOfIf
	lw $t0, 124($sp)  # _tmp10_x
	sw $t0, -8($sp)
	lw $t0, 128($sp)  # _tmp8_j
	sw $t0, -12($sp)
	jal _L6_sort
_L82_endOfIf:
	lw $t2, 132($sp)  # _tmp7_i
	lw $t3, 120($sp)  # _tmp12_y
	slt $t1, $t2, $t3
	sw $t1, 0($sp)  # _tmp83
	lw $t1, 0($sp)  # _tmp83
	li $t2, 0
	beq $t1, $t2, _L85_endOfIf
	lw $t0, 132($sp)  # _tmp7_i
	sw $t0, -8($sp)
	lw $t0, 120($sp)  # _tmp12_y
	sw $t0, -12($sp)
	jal _L6_sort
_L85_endOfIf:
	addiu $sp, $sp, 140
	lw $ra, -4($sp)
	jr $ra
_L86_main:
	sw $ra, -4($sp)
	subiu $sp, $sp, 20
	li $t0, 0
	sw $t0, 12($sp)  # _tmp88_i
	li $t0, 6
	li $t1, 0
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 83
	li $t1, 1
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 79
	li $t1, 2
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 41
	li $t1, 3
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 69
	li $t1, 4
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 99
	li $t1, 5
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 73
	li $t1, 6
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 88
	li $t1, 7
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 49
	li $t1, 8
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 62
	li $t1, 9
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 75
	li $t1, 10
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 43
	li $t1, 11
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 62
	li $t1, 12
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 70
	li $t1, 13
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 77
	li $t1, 14
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 31
	li $t1, 15
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 24
	li $t1, 16
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 35
	li $t1, 17
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 97
	li $t1, 18
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 46
	li $t1, 19
	sll $t1, $t1, 2
	sw $t0, _tmp4_d($t1)
	li $t0, 0
	sw $t0, 12($sp)  # _tmp88_i
	li $t0, 0
	sw $t0, -8($sp)
	li $t0, 19
	sw $t0, -12($sp)
	jal _L6_sort
_L132_whileBeginTest:
	lw $t2, 12($sp)  # _tmp88_i
	li $t3, 20
	slt $t1, $t2, $t3
	sw $t1, 8($sp)  # _tmp134
	lw $t1, 8($sp)  # _tmp134
	li $t2, 0
	beq $t1, $t2, _L136_endOfWhile
	lw $t1, 12($sp)  # _tmp88_i
	sll $t1, $t1, 2
	lw $t0, _tmp4_d($t1)
	sw $t0, 4($sp)  # _tmp138
	la $a0, _str137
	li $v0, PRINT_STR
	syscall
	lw $a0, 4($sp)  # _tmp138
	li $v0, PRINT_INT
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	lw $t2, 12($sp)  # _tmp88_i
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 0($sp)  # _tmp140
	lw $t0, 0($sp)  # _tmp140
	sw $t0, 12($sp)  # _tmp88_i
	j _L132_whileBeginTest
_L136_endOfWhile:
	addiu $sp, $sp, 20
	lw $ra, -4($sp)
	jr $ra
_L2_programBegin:
	jal _L86_main
	j _L142_endOfProgram
_L1_checkConvert:
	sw $ra, -4($sp)
	subiu $sp, $sp, 8
	lw $t1, 0($sp)  # _tmp143
	li $t2, 42
	beq $t1, $t2, _L148_endOfCheck
	lw $t1, 0($sp)  # _tmp143
	li $t2, 43
	beq $t1, $t2, _L148_endOfCheck
	lw $t1, 0($sp)  # _tmp143
	li $t2, 45
	beq $t1, $t2, _L148_endOfCheck
	lw $t1, 0($sp)  # _tmp143
	li $t2, 47
	bge $t1, $t2, _L144_check57
	j _L147_error
_L144_check57:
	lw $t1, 0($sp)  # _tmp143
	li $t2, 57
	ble $t1, $t2, _L148_endOfCheck
	lw $t1, 0($sp)  # _tmp143
	li $t2, 65
	bge $t1, $t2, _L145_check90
	j _L147_error
_L145_check90:
	lw $t1, 0($sp)  # _tmp143
	li $t2, 90
	ble $t1, $t2, _L148_endOfCheck
	lw $t1, 0($sp)  # _tmp143
	li $t2, 97
	bge $t1, $t2, _L146_check122
	j _L147_error
_L146_check122:
	lw $t1, 0($sp)  # _tmp143
	li $t2, 122
	ble $t1, $t2, _L148_endOfCheck
_L147_error:
	la $a0, _err
	li $v0, 4
	syscall
	li $a0, 1
	li $v0, 1
	syscall
	li $a0, 1
	li $v0, 17
	syscall
_L148_endOfCheck:
	addiu $sp, $sp, 8
	lw $ra, -4($sp)
	jr $ra
_L142_endOfProgram:
	li $a0, 0
	li $v0, 17
	syscall
