.eqv PRINT_INT 1
.eqv PRINT_STR 4
.eqv PRINT_CHAR 11
.eqv READ_INT 5
.eqv READ_CHAR 12
.data
	_err: .asciiz "runtime error: "
	_str16: .asciiz " "
	_tmp4_N: .word 20
	_tmp6_d: .space 80
.text
	j _L2_programBegin
_L8_Print:
	sw $ra, -4($sp)
	subiu $sp, $sp, 20
	li $t0, 0
	sw $t0, 12($sp)  # _tmp10_i
	li $t0, 0
	sw $t0, 12($sp)  # _tmp10_i
_L12_whileBeginTest:
	lw $t2, 12($sp)  # _tmp10_i
	lw $t3, _tmp4_N
	slt $t1, $t2, $t3
	sw $t1, 8($sp)  # _tmp13
	lw $t1, 8($sp)  # _tmp13
	li $t2, 0
	beq $t1, $t2, _L15_endOfWhile
	lw $t1, 12($sp)  # _tmp10_i
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 4($sp)  # _tmp17
	la $a0, _str16
	li $v0, PRINT_STR
	syscall
	lw $a0, 4($sp)  # _tmp17
	li $v0, PRINT_INT
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	lw $t2, 12($sp)  # _tmp10_i
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 0($sp)  # _tmp19
	lw $t0, 0($sp)  # _tmp19
	sw $t0, 12($sp)  # _tmp10_i
	j _L12_whileBeginTest
_L15_endOfWhile:
	addiu $sp, $sp, 20
	lw $ra, -4($sp)
	jr $ra
_L21_sort:
	sw $ra, -4($sp)
	subiu $sp, $sp, 96
	li $t0, 0
	sw $t0, 80($sp)  # _tmp25_i
	li $t0, 0
	sw $t0, 76($sp)  # _tmp27_j
	li $t0, 0
	sw $t0, 72($sp)  # _tmp29_z
	li $t0, 0
	sw $t0, 68($sp)  # _tmp31_tmp
	lw $t0, 88($sp)  # _tmp22_start
	sw $t0, 80($sp)  # _tmp25_i
	lw $t0, 84($sp)  # _tmp23_end
	sw $t0, 76($sp)  # _tmp27_j
	lw $t2, 80($sp)  # _tmp25_i
	lw $t3, 76($sp)  # _tmp27_j
	add $t1, $t2, $t3
	sw $t1, 64($sp)  # _tmp32
	lw $t2, 64($sp)  # _tmp32
	li $t3, 2
	div $t1, $t2, $t3
	sw $t1, 60($sp)  # _tmp35
	lw $t1, 60($sp)  # _tmp35
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 56($sp)  # _tmp36
	lw $t0, 56($sp)  # _tmp36
	sw $t0, 72($sp)  # _tmp29_z
_L37_whileBeginTest:
	lw $t2, 80($sp)  # _tmp25_i
	lw $t3, 76($sp)  # _tmp27_j
	sle $t1, $t2, $t3
	sw $t1, 52($sp)  # _tmp38
	lw $t1, 52($sp)  # _tmp38
	li $t2, 0
	beq $t1, $t2, _L40_endOfWhile
_L41_whileBeginTest:
	lw $t1, 80($sp)  # _tmp25_i
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 48($sp)  # _tmp42
	lw $t2, 72($sp)  # _tmp29_z
	lw $t3, 48($sp)  # _tmp42
	sgt $t1, $t2, $t3
	sw $t1, 44($sp)  # _tmp43
	lw $t1, 44($sp)  # _tmp43
	li $t2, 0
	beq $t1, $t2, _L45_endOfWhile
	lw $t2, 80($sp)  # _tmp25_i
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 40($sp)  # _tmp47
	lw $t0, 40($sp)  # _tmp47
	sw $t0, 80($sp)  # _tmp25_i
	j _L41_whileBeginTest
_L45_endOfWhile:
_L49_whileBeginTest:
	lw $t1, 76($sp)  # _tmp27_j
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 36($sp)  # _tmp50
	lw $t2, 72($sp)  # _tmp29_z
	lw $t3, 36($sp)  # _tmp50
	slt $t1, $t2, $t3
	sw $t1, 32($sp)  # _tmp51
	lw $t1, 32($sp)  # _tmp51
	li $t2, 0
	beq $t1, $t2, _L53_endOfWhile
	lw $t2, 76($sp)  # _tmp27_j
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 28($sp)  # _tmp55
	lw $t0, 28($sp)  # _tmp55
	sw $t0, 76($sp)  # _tmp27_j
	j _L49_whileBeginTest
_L53_endOfWhile:
	lw $t2, 80($sp)  # _tmp25_i
	lw $t3, 76($sp)  # _tmp27_j
	sle $t1, $t2, $t3
	sw $t1, 24($sp)  # _tmp57
	lw $t1, 24($sp)  # _tmp57
	li $t2, 0
	beq $t1, $t2, _L59_endOfIf
	lw $t1, 80($sp)  # _tmp25_i
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 20($sp)  # _tmp60
	lw $t0, 20($sp)  # _tmp60
	sw $t0, 68($sp)  # _tmp31_tmp
	lw $t1, 76($sp)  # _tmp27_j
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 16($sp)  # _tmp61
	lw $t0, 16($sp)  # _tmp61
	lw $t1, 80($sp)  # _tmp25_i
	sll $t1, $t1, 2
	sw $t0, _tmp6_d($t1)
	lw $t0, 68($sp)  # _tmp31_tmp
	lw $t1, 76($sp)  # _tmp27_j
	sll $t1, $t1, 2
	sw $t0, _tmp6_d($t1)
	lw $t2, 80($sp)  # _tmp25_i
	li $t3, 1
	add $t1, $t2, $t3
	sw $t1, 12($sp)  # _tmp63
	lw $t0, 12($sp)  # _tmp63
	sw $t0, 80($sp)  # _tmp25_i
	lw $t2, 76($sp)  # _tmp27_j
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 8($sp)  # _tmp66
	lw $t0, 8($sp)  # _tmp66
	sw $t0, 76($sp)  # _tmp27_j
_L59_endOfIf:
	j _L37_whileBeginTest
_L40_endOfWhile:
	lw $t2, 80($sp)  # _tmp25_i
	lw $t3, 84($sp)  # _tmp23_end
	slt $t1, $t2, $t3
	sw $t1, 4($sp)  # _tmp68
	lw $t1, 4($sp)  # _tmp68
	li $t2, 0
	beq $t1, $t2, _L70_endOfIf
	lw $t0, 80($sp)  # _tmp25_i
	sw $t0, -8($sp)
	lw $t0, 84($sp)  # _tmp23_end
	sw $t0, -12($sp)
	jal _L21_sort
_L70_endOfIf:
	lw $t2, 76($sp)  # _tmp27_j
	lw $t3, 88($sp)  # _tmp22_start
	sgt $t1, $t2, $t3
	sw $t1, 0($sp)  # _tmp71
	lw $t1, 0($sp)  # _tmp71
	li $t2, 0
	beq $t1, $t2, _L73_endOfIf
	lw $t0, 88($sp)  # _tmp22_start
	sw $t0, -8($sp)
	lw $t0, 76($sp)  # _tmp27_j
	sw $t0, -12($sp)
	jal _L21_sort
_L73_endOfIf:
	addiu $sp, $sp, 96
	lw $ra, -4($sp)
	jr $ra
_L74_main:
	sw $ra, -4($sp)
	subiu $sp, $sp, 64
	li $t0, 0
	sw $t0, 56($sp)  # _tmp76_i
	li $t0, 0
	li $t1, 0
	sll $t1, $t1, 2
	sw $t0, _tmp6_d($t1)
	li $t0, 2
	sw $t0, 56($sp)  # _tmp76_i
_L80_whileBeginTest:
	lw $t2, _tmp4_N
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 52($sp)  # _tmp82
	lw $t2, 56($sp)  # _tmp76_i
	lw $t3, 52($sp)  # _tmp82
	sle $t1, $t2, $t3
	sw $t1, 48($sp)  # _tmp84
	lw $t1, 48($sp)  # _tmp84
	li $t2, 0
	beq $t1, $t2, _L86_endOfWhile
	lw $t2, 56($sp)  # _tmp76_i
	li $t3, 2
	sub $t1, $t2, $t3
	sw $t1, 44($sp)  # _tmp88
	lw $t1, 44($sp)  # _tmp88
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 40($sp)  # _tmp90
	lw $t2, 40($sp)  # _tmp90
	li $t3, 2
	add $t1, $t2, $t3
	sw $t1, 36($sp)  # _tmp92
	lw $t0, 36($sp)  # _tmp92
	lw $t1, 56($sp)  # _tmp76_i
	sll $t1, $t1, 2
	sw $t0, _tmp6_d($t1)
	lw $t2, 56($sp)  # _tmp76_i
	li $t3, 2
	add $t1, $t2, $t3
	sw $t1, 32($sp)  # _tmp95
	lw $t0, 32($sp)  # _tmp95
	sw $t0, 56($sp)  # _tmp76_i
	j _L80_whileBeginTest
_L86_endOfWhile:
	lw $t2, _tmp4_N
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 28($sp)  # _tmp99
	lw $t0, 28($sp)  # _tmp99
	li $t1, 1
	sll $t1, $t1, 2
	sw $t0, _tmp6_d($t1)
	li $t0, 3
	sw $t0, 56($sp)  # _tmp76_i
_L102_whileBeginTest:
	lw $t2, _tmp4_N
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 24($sp)  # _tmp104
	lw $t2, 56($sp)  # _tmp76_i
	lw $t3, 24($sp)  # _tmp104
	sle $t1, $t2, $t3
	sw $t1, 20($sp)  # _tmp106
	lw $t1, 20($sp)  # _tmp106
	li $t2, 0
	beq $t1, $t2, _L108_endOfWhile
	lw $t2, 56($sp)  # _tmp76_i
	li $t3, 2
	sub $t1, $t2, $t3
	sw $t1, 16($sp)  # _tmp110
	lw $t1, 16($sp)  # _tmp110
	sll $t1, $t1, 2
	lw $t0, _tmp6_d($t1)
	sw $t0, 12($sp)  # _tmp112
	lw $t2, 12($sp)  # _tmp112
	li $t3, 2
	sub $t1, $t2, $t3
	sw $t1, 8($sp)  # _tmp114
	lw $t0, 8($sp)  # _tmp114
	lw $t1, 56($sp)  # _tmp76_i
	sll $t1, $t1, 2
	sw $t0, _tmp6_d($t1)
	lw $t2, 56($sp)  # _tmp76_i
	li $t3, 2
	add $t1, $t2, $t3
	sw $t1, 4($sp)  # _tmp117
	lw $t0, 4($sp)  # _tmp117
	sw $t0, 56($sp)  # _tmp76_i
	j _L102_whileBeginTest
_L108_endOfWhile:
	jal _L8_Print
	lw $t2, _tmp4_N
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 0($sp)  # _tmp121
	li $t0, 0
	sw $t0, -8($sp)
	lw $t0, 0($sp)  # _tmp121
	sw $t0, -12($sp)
	jal _L21_sort
	jal _L8_Print
	addiu $sp, $sp, 64
	lw $ra, -4($sp)
	jr $ra
_L2_programBegin:
	jal _L74_main
	j _L123_endOfProgram
_L1_checkConvert:
	sw $ra, -4($sp)
	subiu $sp, $sp, 8
	lw $t1, 0($sp)  # _tmp124
	li $t2, 42
	beq $t1, $t2, _L129_endOfCheck
	lw $t1, 0($sp)  # _tmp124
	li $t2, 43
	beq $t1, $t2, _L129_endOfCheck
	lw $t1, 0($sp)  # _tmp124
	li $t2, 45
	beq $t1, $t2, _L129_endOfCheck
	lw $t1, 0($sp)  # _tmp124
	li $t2, 47
	bge $t1, $t2, _L125_check57
	j _L128_error
_L125_check57:
	lw $t1, 0($sp)  # _tmp124
	li $t2, 57
	ble $t1, $t2, _L129_endOfCheck
	lw $t1, 0($sp)  # _tmp124
	li $t2, 65
	bge $t1, $t2, _L126_check90
	j _L128_error
_L126_check90:
	lw $t1, 0($sp)  # _tmp124
	li $t2, 90
	ble $t1, $t2, _L129_endOfCheck
	lw $t1, 0($sp)  # _tmp124
	li $t2, 97
	bge $t1, $t2, _L127_check122
	j _L128_error
_L127_check122:
	lw $t1, 0($sp)  # _tmp124
	li $t2, 122
	ble $t1, $t2, _L129_endOfCheck
_L128_error:
	la $a0, _err
	li $v0, 4
	syscall
	li $a0, 1
	li $v0, 1
	syscall
	li $a0, 1
	li $v0, 17
	syscall
_L129_endOfCheck:
	addiu $sp, $sp, 8
	lw $ra, -4($sp)
	jr $ra
_L123_endOfProgram:
	li $a0, 0
	li $v0, 17
	syscall
