#include<stdio.h>
const char errorFlag='/';
int f[50];

char check(int num){
  if(num<=0) return(errorFlag);
  return('+');
}

void error(char result, int num){
  printf("invalid num! num must larger than 0.\n");
  printf("found %d\n",num);
}

int fib(int num){
  char result;
  result = check(num);
  switch(result){
    case '/':{
      error(result, num);
	  return(0);
    }
	case '+':{
	  if(num>=3){
		if(f[num-2]==0) f[num-2]=fib(num-2);
		if(f[num-1]==0) f[num-1]=fib(num-1);
		f[num]=f[num-1]+f[num-2];
		return(f[num]);
	  }
	  return(1);;
	}
	default: printf("should not happen\n");
  }
}

void main(){
  int n;
  f[0]=-1;
  f[+1]=1;
  f[2]=+1;
  printf("calc fib num, input:");
  scanf("%d",&n);
  printf("try calc fib %d\n",n);
  printf("result is %d\n", fib(n));
}