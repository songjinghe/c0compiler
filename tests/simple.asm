.eqv PRINT_INT  1
.eqv PRINT_STR  4
.eqv PRINT_CHAR 11
.eqv READ_INT   5
.eqv READ_CHAR  12

.data
	_err: .asciiz "runtime error: "
.text
	j _L2_programBegin
_L4_f:
	sw $ra, -4($sp)
	subiu $sp, $sp, 24
	lw $t2, 16($sp)  # _tmp5_n
	li $t3, 1
	sgt $t1, $t2, $t3
	sw $t1, 12($sp)  # _tmp7
	lw $t1, 12($sp)  # _tmp7
	li $t2, 0
	beq $t1, $t2, _L10_else
	lw $t2, 16($sp)  # _tmp5_n
	li $t3, 1
	sub $t1, $t2, $t3
	sw $t1, 8($sp)  # _tmp12
	lw $t0, 8($sp)  # _tmp12
	sw $t0, -8($sp)
	jal _L4_f
	sw $v0, 4($sp)  # _tmp14
	lw $t2, 4($sp)  # _tmp14
	lw $t3, 16($sp)  # _tmp5_n
	add $t1, $t2, $t3
	sw $t1, 0($sp)  # _tmp15
	lw $v0, 0($sp)  # _tmp15
	addiu $sp, $sp, 24
	lw $ra, -4($sp)
	jr $ra
	j _L9_endOfIf
_L10_else:
	li $v0, 1
	addiu $sp, $sp, 24
	lw $ra, -4($sp)
	jr $ra
_L9_endOfIf:
	la $a0, _err
	li $v0, 4
	syscall
	li $a0, 0
	li $v0, 1
	syscall
	li $a0, 0
	li $v0, 17
	syscall
_L19_main:
	sw $ra, -4($sp)
	subiu $sp, $sp, 8
	li $t0, 100
	sw $t0, -8($sp)
	jal _L4_f
	sw $v0, 0($sp)  # _tmp21
	lw $a0, 0($sp)  # _tmp21
	li $v0, PRINT_INT
	syscall
	li $a0, 10  # print new line
	li $v0, PRINT_CHAR
	syscall
	addiu $sp, $sp, 8
	lw $ra, -4($sp)
	jr $ra
_L2_programBegin:
	jal _L19_main
	j _L22_endOfProgram
_L1_checkConvert:
	sw $ra, -4($sp)
	subiu $sp, $sp, 8
	lw $t1, 0($sp)  # _tmp23
	li $t2, 42
	beq $t1, $t2, _L28_endOfCheck
	lw $t1, 0($sp)  # _tmp23
	li $t2, 43
	beq $t1, $t2, _L28_endOfCheck
	lw $t1, 0($sp)  # _tmp23
	li $t2, 45
	beq $t1, $t2, _L28_endOfCheck
	lw $t1, 0($sp)  # _tmp23
	li $t2, 47
	bge $t1, $t2, _L24_check57
	j _L27_error
_L24_check57:
	lw $t1, 0($sp)  # _tmp23
	li $t2, 57
	ble $t1, $t2, _L28_endOfCheck
	lw $t1, 0($sp)  # _tmp23
	li $t2, 65
	bge $t1, $t2, _L25_check90
	j _L27_error
_L25_check90:
	lw $t1, 0($sp)  # _tmp23
	li $t2, 90
	ble $t1, $t2, _L28_endOfCheck
	lw $t1, 0($sp)  # _tmp23
	li $t2, 97
	bge $t1, $t2, _L26_check122
	j _L27_error
_L26_check122:
	lw $t1, 0($sp)  # _tmp23
	li $t2, 122
	ble $t1, $t2, _L28_endOfCheck
_L27_error:
	la $a0, _err
	li $v0, 4
	syscall
	li $a0, 1
	li $v0, 1
	syscall
	li $a0, 1
	li $v0, 17
	syscall
_L28_endOfCheck:
	addiu $sp, $sp, 8
	lw $ra, -4($sp)
	jr $ra
_L22_endOfProgram:
	li $a0, 0
	li $v0, 17
	syscall
