#!/usr/bin/python

import os
import sys,time,re
import subprocess,threading

class RunAsync(threading.Thread):
    def __init__(self,function=None,arg=None,CallBackFunction=None,callBackArg=None,count=True,DoneFunction=None,msg=''):
        threading.Thread.__init__(self)
        self.f = function
        self.f_arg = arg
        self.f_callback = CallBackFunction
        self.f_callback_arg = callBackArg
        self.f_done=DoneFunction
        self.msg=msg
        self.daemon=True
        self.start()

    def run(self):
        begin_time=time.time()
        if self.f_arg is None:self.f()
        else:  self.f(self.f_arg)
        end_time=time.time()
        print('%d' % (end_time-begin_time))


def RunSubProcess(p):
    os.system(p)
    return


def ForkRun(p,s=None):
    if s is None:
        r=os.fork()
        if r==0:
            os.system(p)
            sys.exit()
        else:
            return
    else:
        os.system(p)
    return

def GetCmdOutput(commandString, input):
    onError='throw'
    msg=False
    try:
        if msg: print('command:'+commandString+'\n','cmd')
        proc = subprocess.Popen([commandString], stdin=subprocess.PIPE, stdout=subprocess.PIPE,shell=True )
        data = proc.communicate(input)
        outputString = data[0]
        errorString = data[1]
        if errorString is None:pass
        else:
            if msg: print(errorString,'error')
            if msg: print('\n')
        return proc.returncode, commandString, outputString

    except subprocess.CalledProcessError,e:
        print(e.output,'error')
        if onError=='throw':
            pass
        else:pass
        return e.returncode, e.cmd, e.output

def Now():
    t=time.localtime()
    tmp="%s-%s-%s %s:%s:%s" % (t[0],t[1],t[2],t[3],t[4],t[5])
    return tmp

def DoNothing(): print('do nothing')


if __name__=="__main__":
    for i in range(-4, 50):
        rt, cmd, out = GetCmdOutput('./a.out', str(i))
        print out