//
// Created by song on 16-11-18.
//

#ifndef C0COMPILER_UTILS_H
#define C0COMPILER_UTILS_H

#include <algorithm>
#include <set>
#include "../front/Token.h"

class Utils{
public:
    static set<char> string2set(const char *str){
        set<char> set1;
        char * p = (char *) str;
        while(*p!='\0'){
            set1.insert(*p);
            p++;
        }
        return set1;
    }

    static set<TokenType> mergeSet(set<TokenType> set0, set<TokenType> set1) {
        set<TokenType> final(set0);
        set<TokenType>::const_iterator iter;
        for(iter = set1.begin(); iter!=set1.end(); iter++){
            if(final.count(*iter)==0){
                final.insert(*iter);
            }
        }
        return final;
    }

    static void split(const std::string &s, char delim, std::vector<std::string> &elems) {
        std::stringstream ss;
        ss.str(s);
        std::string item;
        while(std::getline(ss, item, delim)) {
            elems.push_back(item);
        }
    }

    static bool replace(std::string& str, const std::string& from, const std::string& to) {
        size_t start_pos = str.find(from);
        if(start_pos == std::string::npos)
            return false;
        str.replace(start_pos, from.length(), to);
        return true;
    }

    static string removeSpaces(string &str){
        std::string::iterator end_pos = std::remove(str.begin(), str.end(), ' ');
        str.erase(end_pos, str.end());
        return str;
    }

};


#endif //C0COMPILER_UTILS_H
