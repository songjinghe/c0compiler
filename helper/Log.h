//
// Created by song on 16-11-18.
//

#ifndef C0COMPILER_LOG_H
#define C0COMPILER_LOG_H


#include <iostream>

class Log {
public:
    static void trace(const char *msg){
        std::cout<<"[TRACE] "<<msg<<std::endl;
    }
};


#endif //C0COMPILER_LOG_H
