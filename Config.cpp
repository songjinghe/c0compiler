//
// Created by song on 16-12-28.
//
#include <string>
#include "Config.h"

using std::string;


string Config::sourceFileName;
string Config::asmFileName;
bool Config::showAllError=false;
bool Config::printAST=false;
bool Config::printSource=false;
bool Config::printTokens=false;
bool Config::printSymbolTable=false;
bool Config::printASTDetail=false;
bool Config::printASM=false;
bool Config::printIR=false;
bool Config::irSymbolTable=false;
bool Config::levelOneOptimize=false;
bool Config::printBasicBlocks = false;
bool Config::printControlFlowChart = false;
bool Config::printDefUseChains = false;
bool Config::printMergedDFChain = false;
bool Config::printConflictGraph = false;