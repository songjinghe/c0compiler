//
// Created by song on 16-12-12.
//

#ifndef C0COMPILER_ASTTRANSFORMER_H
#define C0COMPILER_ASTTRANSFORMER_H

#include "ASTVisitor.h"

class ASTTransformer: public ASTVisitor{

protected:
    bool condition(ASTNode &node, bool postVisit) override {
        if(!postVisit){
            ASTNode * child = node.children[0];
            ASTNode * parent = node.parent;
            parent->children[0] = child;
        }
        return false;
    }

    bool switchStmt(ASTNode &node, bool postVisit) override {
        long len = node.children.size();
        if(len>0 && node.children[len-1]->astType!=DefaultSegment){
            // the last one is not a default statement
            // so we add an empty one
            node.addChild(new ASTNode(DefaultSegment));
        }
        return false;
    }

public:
    ASTTransformer(){
        this->defaultAction = true;
    }

};
#endif //C0COMPILER_ASTTRANSFORMER_H
