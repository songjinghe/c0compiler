//
// Created by song on 16-12-20.
//

#ifndef C0COMPILER_SIMPLEIRVISITOR_H
#define C0COMPILER_SIMPLEIRVISITOR_H


#include <set>
#include "IRVisitor.h"
#include "MIPsIns.h"
#include "../helper/Error.h"

class SimpleIRVisitor: public IRVisitor {
public:
    SimpleIRVisitor(set<TmpVar*> globalVars, MIPsIns &ins):
            globalVars(globalVars),
            a(ins)
    {
        paramCount = 1;
    }

private:
    TmpVar* functionLabel;
    TmpVar* left;
    TmpVar* right;
    TmpVar* result;
    MIPsIns &a;
    int paramCount;
    set<TmpVar*> globalVars;
protected:
    ReturnedObj *add(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.add(T1, T2, T3);
        saveStackVar(result, T1);
    }

    ReturnedObj *sub(IR *ir) override {
        load(left, T2);
        load(right, T3);
        a.sub(T1, T2, T3);
        saveStackVar(result, T1);
    }

    ReturnedObj *multiple(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.mul(T1, T2, T3);
        saveStackVar(result, T1);
    }

    ReturnedObj *division(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.div(T1, T2, T3);
        saveStackVar(result, T1);
    }

    ReturnedObj *jmp(IR *ir) override {
        a.j(result->name());
    }

    ReturnedObj *jge(IR *ir) override {
        load(left, T1);
        load(right,T2);
        a.bge(T1, T2, result->name());
    }

    ReturnedObj *jg(IR *ir) override {
        load(left, T1);
        load(right,T2);
        a.bgt(T1, T2, result->name());
    }

    ReturnedObj *jne(IR *ir) override {
        load(left, T1);
        load(right,T2);
        a.bne(T1, T2, result->name());
    }

    ReturnedObj *je(IR *ir) override {
        load(left, T1);
        load(right,T2);
        a.beq(T1, T2, result->name());
    }

    ReturnedObj *jle(IR *ir) override {
        load(left, T1);
        load(right,T2);
        a.ble(T1, T2, result->name());
    }

    ReturnedObj *jl(IR *ir) override {
        load(left, T1);
        load(right,T2);
        a.bge(T1, T2, result->name());
    }

    ReturnedObj *assign(IR *ir) override {
        load(left, T0);
        if(globalVars.count(result)>0){ //global var
            a.sw( T0, result->name());
        }else{
            saveStackVar(result, T0);
        }
    }

    ReturnedObj *readInt(IR *ir) override {
        a.li(V0, "READ_INT");
        a.syscall();
        if(globalVars.count(result)>0){
            a.sw( V0, result->name() );
        }else{
            saveStackVar(result, V0);
        }
    }

    ReturnedObj *readChar(IR *ir) override {
        a.li ( V0, "READ_CHAR" );
        a.syscall();
        if(globalVars.count(result)>0){
            a.sw( V0, result->name());
        }else{
            saveStackVar(result, V0);
        }
    }

    ReturnedObj *writeStr(IR *ir) override {
        a.la(A0, left->name());
        a.li(V0, "PRINT_STR");
        a.syscall();
    }

    ReturnedObj *writeInt(IR *ir) override {
        load(left, A0);
        a.li(V0, "PRINT_INT");
        a.syscall();
    }

    ReturnedObj *writeChar(IR *ir) override {
        load(left, A0);
        a.li( V0, "PRINT_CHAR");
        a.syscall();
    }

    ReturnedObj *writeln(IR *ir) override {
        a.li( A0, ((int)'\n') );a.comment("print new line");
        a.li( V0, "PRINT_CHAR");
        a.syscall();
    }

    ReturnedObj *globalArray(IR *ir) override {

    }

    ReturnedObj *stackArray(IR *ir) override {

    }

    ReturnedObj *noop(IR *ir) override {

    }

    ReturnedObj *pushParam(IR *ir) override {
        load(left, T0);
        a.sw(T0, SP, (-4) * (paramCount + 1));
        paramCount++;
    }

    ReturnedObj *popParam(IR *ir) override {

    }

    ReturnedObj *error(IR *ir) override {
        a.la(A0, "_err");
        a.li(V0, 4);
        a.syscall();
        load(left, A0);
        a.li(V0, 1);
        a.syscall();
        load(left, A0);
        a.li(V0, 17);
        a.syscall();
    }

    ReturnedObj *call(IR *ir) override {
        a.jal(left->name());
        if(result!=NULL){
            saveStackVar(result, V0);
        }
        paramCount=1;
    }

    ReturnedObj *functionReturn(IR *ir) override {
        if(ir->left!=NULL){
            load(left, V0);
        }
        a.addiu(SP, SP, functionLabel->value);
        a.lw(  RA, SP, -4 );
        a.jr( RA );
    }

    ReturnedObj *deref(IR *ir) override {
        // offset in t1
        load(right, T1);
        a.sll(T1, T1, 2); // shift 2 (logical) bit == multiple by 4
        if(globalVars.count(left)){ //global var
            a.lw(T0, left->name(), T1);
        }else{
            a.addi( T1, SP, left->value);
            a.lw( T0, T1);
        }
        saveStackVar(result, T0);
    }

    ReturnedObj *assignArray(IR *ir) override {
        load(result, T0);
        load(right, T1);
        a.sll(T1, T1, 2); // shift left (logical) 2 bit == multiple by 4
        if(globalVars.count(left)){ //global var
            a.sw( T0, left->name(), T1);
        }else{
            a.addi(T1, SP, left->value); //add offset of array in function frame
            a.sw( T0, T1);
        }
    }

    ReturnedObj *functionBegin(IR *ir) override {
        functionLabel = ir->label;
        a.sw(RA, SP, -4);
        a.subiu(SP, SP, functionLabel->value);
        return NULL;
    }

    ReturnedObj *functionEnd(IR *ir) override {

    }

    ReturnedObj *sge(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.sge(T1, T2, T3);
        saveStackVar(result,T1);
    }

    ReturnedObj *sg(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.sgt(T1, T2, T3);
        saveStackVar(result,T1);
    }

    ReturnedObj *sne(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.sne(T1, T2, T3);
        saveStackVar(result,T1);
    }

    ReturnedObj *se(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.seq(T1, T2, T3);
        saveStackVar(result,T1);
    }

    ReturnedObj *sle(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.sle(T1, T2, T3);
        saveStackVar(result,T1);
    }

    ReturnedObj *sl(IR *ir) override {
        load(left, T2);
        load(right,T3);
        a.slt(T1, T2, T3);
        saveStackVar(result,T1);
    }

    void load(TmpVar* v, Register target){
        switch(v->type){
            case TmpVar::VAR :
                if(globalVars.count(v)>0){ // global variable
                    a.lw( target, v->name());
                }else{
                    a.lw( target, SP, v->value );
                    a.comment(v->name());
                }
                break;
            case TmpVar::VALUE:
                a.li( target , v->value ) ;
                break;
            default:
                Error::nextErrorDetail<< "try load a var type is not var or value";
                Error::internal(Error::Should_Not_Happen);
        }
    }

    void saveStackVar(TmpVar* v, Register source){
        if(v->type==TmpVar::VAR) {
            if (globalVars.count(v)==0) { // local variable
                a.sw(source, SP, v->value);
                a.comment(v->name());
                return;
            }
        }
        Error::nextErrorDetail<< "try load a var type is not var or value";
        Error::internal(Error::Should_Not_Happen);
    }

public:

    ReturnedObj *visit(IR *ir) override {
        left = ir->left;
        right = ir->right;
        result = ir->result;
        return IRVisitor::visit(ir);
    }

};


#endif //C0COMPILER_SIMPLEIRVISITOR_H
