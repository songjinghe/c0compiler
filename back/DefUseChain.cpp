//
// Created by song on 16-12-30.
//

#include "DefUseChain.h"
int DefUseChain::Node::nextId = 0;
map<IR*, DefUseChain*> DefUseChain::irMap;
map<TmpVar*, set<DefUseChain*>> DefUseChain::groupByVar;
vector<DefUseChain::Node*> DefUseChain::Node::list;
map<IR*, DefUseChain::Node*> DefUseChain::Node::ir2Node;