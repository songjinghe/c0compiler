//
// Created by song on 16-12-20.
//

#ifndef C0COMPILER_IRVISITOR_H
#define C0COMPILER_IRVISITOR_H


class ReturnedObj{

};

class IRVisitor {
protected:

    virtual ReturnedObj* add(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* sub(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* multiple(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *division(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *jmp(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *jge(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *jg(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *jne(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *je(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *jle(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *jl(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *assign(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *readInt(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *readChar(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *writeStr(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *writeInt(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *writeChar(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *writeln(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *globalArray(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *stackArray(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *noop(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *pushParam(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *popParam(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *error(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *call(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *functionReturn(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *deref(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *assignArray(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *functionBegin(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj *functionEnd(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* sge(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* sg(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* sne(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* se(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* sle(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* sl(IR *ir) {
        return NULL;
    }

    virtual ReturnedObj* visit(IR* ir){
        switch(ir->op){
            case ADD:
                return add(ir);
            case SUB:
                return sub(ir);
            case MUL:
                return multiple(ir);
            case DIV:
                return division(ir);
            case JMP:
                return jmp(ir);
            case JGE:
                return jge(ir);
            case JG:
                return jg(ir);
            case JNE:
                return jne(ir);
            case JE:
                return je(ir);
            case JLE:
                return jle(ir);
            case JL:
                return jl(ir);
            case ASSIGN:
                return assign(ir);
            case READ:
                return readInt(ir);
            case READCHAR:
                return readChar(ir);
            case WRITESTR:
                return writeStr(ir);
            case WRITECHAR:
                return writeChar(ir);
            case WRITE:
                return writeInt(ir);
            case WRITELN:
                return writeln(ir);
            case ARRAY:
                return globalArray(ir);
            case PUSHARR:
                return stackArray(ir);
            case NOOP:
                return noop(ir);
            case PUSHPARAM:
                return pushParam(ir);
            case POPPARAM:
                return popParam(ir);
            case ERROR:
                return error(ir);
            case CALL:
                return call(ir);
            case RETURN:
                return functionReturn(ir);
            case DEFREF:
                return deref(ir);
            case ASSIGNARR:
                return assignArray(ir);
            case FUNBEGIN:
                return functionBegin(ir);
            case FUNEND:
                return functionEnd(ir);
            case SGE:
                return sge(ir);
            case SG:
                return sg(ir);
            case SNE:
                return sne(ir);
            case SE:
                return se(ir);
            case SLE:
                return sle(ir);
            case SL:
                return sl(ir);
        }
    }
};


#endif //C0COMPILER_IRVISITOR_H
